import React, { Suspense } from "react";
import {
  createInstance,
  OptimizelyProvider,
  OptimizelyFeature
} from "@optimizely/react-sdk";
import { Account } from "./Account";
const optimizely = createInstance({
  sdkKey: "SsDu3WQjwRb8qHHBkZPgZs"
});

function App() {
  const CreditCard = React.lazy(() => import("./CreditCard.js"));
  // const [Card, setCard] = useState(React.lazy(() => import("./Account")));

  // const loadCard = () =>
  //   setCard(React.lazy(() => import("./Loading")));

  return (
    <OptimizelyProvider
      optimizely={optimizely}
      user={{
        id: "user123"
      }}
    >
      <OptimizelyFeature feature="credit-card">
        {isEnabled =>
          isEnabled ? (
            <Suspense fallback={<div>Loading...</div>}>
              <CreditCard />
            </Suspense>
          ) : (
            <div>Nope!</div>
          )
        }
      </OptimizelyFeature>
      <Account></Account>
    </OptimizelyProvider>
  );
}

export default App;
